<?php 

mb_internal_encoding("UTF-8");

$npsId = $_REQUEST['showdetail'];
$npsDetail = NpsList::find($npsId);
$answersCount = NpsAnswers::query()->where('npsId', '=', $npsId)->count();
if($answersCount !== 0){
	$minDateQ = NpsAnswers::query()->where('npsId', '=', $npsId)->first()->npsAnswerDate;
	$maxDateQ = NpsAnswers::query()->where('npsId', '=', $npsId)->orderBy('npsId','desc')->first()->npsAnswerDate;
	$minDate = date('Y-m-d', strtotime($minDateQ));
	$maxDate = date('Y-m-d', strtotime($maxDateQ));
}



?>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url(__FILE__)."/css/nps.css" ?>">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<br>

<a href="<?php echo admin_url("admin.php?page=pashkov-nps%2Fpashkov-nps-admin-list.php") ?>">Назад</a>

<br>

<h2>Статистика по опросу "<?php echo $npsDetail->npsTitle?>" <br> </h2>
<h4>id: <?php echo $npsDetail->npsId ?></h4>
<br>
Всего ответов: <?php echo $answersCount ?><br><br>
<div>
       Выбрать период: 
        <input type="date" id="start" name="npsPeriod"
              value="<?php echo $minDate ?>"
               min="<?php echo $minDate ?>" max="<?php echo $maxDate ?>" />
        <input type="date" id="end" name="npsPeriod"
               value="<?php echo $maxDate ?>"
               min="<?php echo $minDate ?>" max="<?php echo $maxDate ?>" />
        <button class="btn btn-sm npsPickPeriod" >Выбрать</button>

<br>
<br>
<div class="detailAnsWrapper">
<?php 
$query = NpsAnswers::query()
->where('npsId', '=', $npsId);
if(isset($_REQUEST['from'])){
	$from = $_REQUEST['from'];
	$query->where('npsAnswerDate', '>', $from);
}
if (isset($_REQUEST['to'])){
	$to = $_REQUEST['to'];
	$query->where('npsAnswerDate', '<', $to);
}
if(isset($_REQUEST['from']) && isset($_REQUEST['to'])){
	$query->where('npsAnswerDate', '>=', $from);
	$query->where('npsAnswerDate', '<=', $to);
}
	
$answers = $query->get();
$answers_arrs = [];
foreach ($answers as $i) {
	array_push($answers_arrs, $i->npsAnswer);
}
$countarr = [];
$ansarr = [];
$finarr = array_count_values($answers_arrs);
echo '<br>';
for($i = 1; $i <=  $npsDetail->npsMaxRange ; $i++) :
	$ans = isset( $finarr[$i]) ?  $finarr[$i]: 0;
	if($answersCount !== 0){
		$per = round(100 * $ans / $answersCount);
	} else {
		$per = 0;
	} 
	array_push($countarr, $i);
	$answer = isset($finarr[$i]) ? $finarr[$i]:0;
	array_push($ansarr, $answer);
	?>

	<br>
	<div class="detailAnsBox" style="background-color: rgba(51, 117, 224, <?php if($per == 100) {echo "1";}else if($per < 10){echo "0.0".$per;} else {echo "0.".$per;} ?>)">
		<div class="dab_ans"><?php echo $ans ?> (<?php echo $per?>%)</div>
		<div class="dab_count"><?php echo $i ?></div>

	</div>

<?php endfor; ?>

</div>
<br>
<button class="btn btn-sm nps_export_csv" data-id="<?php echo $npsId ?>">Экспорт в CSV</button>
<br>

<script type="text/javascript">
	(function($){
$(".npsPickPeriod").on('click', function(){
	var start = $("input#start").val();
	var end = $("input#end").val();
	var currenturl = window.location;
	var newurl = currenturl + "&from=" + start + "&to=" + end;
	window.location = newurl;
})


$(".nps_export_csv").on('click', function(){
	var npsId = <?php echo $_REQUEST['showdetail']; ?>;
	var currenturl = window.location;
	var newurl = currenturl + "&exportcsv="+npsId;
	window.location = newurl;
})


})(jQuery);


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<canvas id="myChart" width="500" height="180"></canvas>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myPieChart = new Chart(ctx,{
    type: 'pie',
    data: {
        labels: <?php echo json_encode($countarr) ?>,
        datasets: [{
            label: 'Голосов',
            data: <?php echo json_encode($ansarr) ?>,
            backgroundColor: [
               'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],

        }]
    }
});

</script>