<?php

use \Illuminate\Database\Eloquent\Model;
class NpsAnswers extends Model
{
	protected $table = 'pashkov_nps_answers';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = [
		'npsId',
		'npsAnswer', 
		'npsAnswerDate',

	];
}