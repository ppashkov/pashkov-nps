<?php
use \Illuminate\Database\Eloquent\Model;

class NpsList extends Model
{
	protected $table = 'pashkov_nps_list';
	protected $primaryKey = 'npsId';
	public $timestamps = false;
	protected $fillable = [ 
		'npsTitle',
		'npsTimeout',
		'npsRangeMark',
		'npsCreateDate'
	];
}

