<?php



$npsStatus = isset($_COOKIE['nps-status']) ? false:true;
if($npsStatus == 0){
	return false;
}

$query = NpsList::query();
if(isset($scId)){
	$query->where('npsId', '=', $scId);
}else {
	$query->where('visible', '=', 1)
	->where('type', '=', 'Shortcode')
	->inRandomOrder();
}
if(isset($_COOKIE['nps-data'])){
	$cookie = $_COOKIE['nps-data'];
	$cookie = stripslashes($cookie);
	$npsData = json_decode($cookie);
	$npsJsonData = json_decode($_COOKIE['nps-data'], true);
	$query->whereNotIn('npsId', $npsData);
}
$model = $query->first();


if($model === null){
	return false;
}

?>

<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url(__FILE__)."/css/nps.css" ?>">

<div class="shortcode_wrapper">
	<div class="sc_title" data-title="<?php echo $model['npsTitle']?>" data-id="<?php echo $model['npsId'] ?>">
		<?php echo $model['npsTitle']?>
	</div>
	<div class="sc_rangerow">
		<form id="scForm" class="sc_range_row">
					<?php foreach (range(1, $model->npsMaxRange) as $i ): ?>


				
				
				<div class="markbox">
					<div class="mb_input"><input type="radio" class="sc_radio" name="npsMarkSc" data-mark="<?php echo $i ?>" ></div>
					<div class="mb_label"><label><?php echo $i ?></label></div>
				</div>

				<?php endforeach; ?>

				</form>

	</div>
	<div class="nps_sc_succes" style="display: none;">
			Спасибо за ответ! :)

				</div>
	<div class="sc_button_row">
		<button class="btn btn-success nps_sc_submit">Отправить</button>
	</div>
	<br>
	<div class="nps_sc_error" style="display: none;">Оценка не выбрана</div>
</div>

<script type="text/javascript">
	(function($){
 	

function nps_sc_succes_hide(){
	$(".sc_range_row").hide();
		$(".nps_sc_succes").show();
		setTimeout(function(){
		$(".shortcode_wrapper").hide('slow');

		}, 800);

}

function nps_sc_hide(){
	$(".shortcode_wrapper").hide();
}



$(".nps_sc_submit").on('click', function(){
	$(".nps_sc_error").hide();
	var npsId = $(".sc_title").attr("data-id"); 
	var npsMark = $('input[name=npsMarkSc]:checked', '#scForm').attr("data-mark");
	if($("input[name=npsMarkSc]").is(':checked')){
	
	$.ajax({
	      method: 'POST',
	      url: window.ajaxurl,
	      data: {
	      	action: "npsPopupSubmit",
	      	id: npsId,
	      	mark: npsMark
	      },
	      succes: nps_sc_succes_hide()
	      

	  
		});
} else{
	$(".nps_sc_error").show();
}

})

 	})(jQuery);
</script>