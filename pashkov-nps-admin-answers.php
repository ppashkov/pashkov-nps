
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


<br>
<h2>Список ответов</h2>
<table class="table table-hover">
	<thead>
		<tr>
			<td><b>ID</b></td>
			<td><b>Оценка</b></td>
			<td><b>Дата ответа</b></td>

		</tr>
	</thead>
	<tbody>
		<?php 
		$npsanswers = NpsAnswers::query()->orderBy('npsAnswerDate', 'desc')->get();

		foreach($npsanswers as $i): ?>
			<tr>
				<td><?php echo $i->npsId ?></td>
			<td><?php echo $i->npsAnswer ?></td>
			<td><?php echo $i->npsAnswerDate ?></td>

			</tr>


		<?php endforeach; ?>
		

	</tbody>
</table>


