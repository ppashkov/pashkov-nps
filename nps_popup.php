<?php


if( is_single()){
$content = get_post();

if(has_shortcode($content->post_content, 'p_nps')){
	return false;
}	
}


$npsStatus = isset($_COOKIE['nps-status']) ? false:true;
if($npsStatus === false){
	return false;
}


$query = NpsList::query()
	->where('visible', '=', 1)
	->where('type', '=', 'Popup')
	->inRandomOrder();
if(isset($_COOKIE['nps-data'])){
	$cookie = $_COOKIE['nps-data'];
	$cookie = stripslashes($cookie);
	$npsData = json_decode($cookie);
	$npsJsonData = json_decode($_COOKIE['nps-data'], true);
	$query->whereNotIn('npsId', $npsData);
}
$model = $query->first();


if($model === null){
	return false;
}

?>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url(__FILE__)."/css/nps.css" ?>">
<div class="nps_popup">
	<div class="nps_popup_overlay">
		<div class="nps_popup_content">
			<div class="c_title" data-title="<?php echo $model['npsTitle']?>" data-id="<?php echo $model['npsId'] ?>">
				<?php echo $model['npsTitle']?>
			</div>
			<div class="c_range_row">
				<form id="popupForm" class="c_range_row">
					<?php foreach (range(1, $model->npsMaxRange) as $i ): ?>
						<div class="markbox">
						<div class="mb_input"><input type="radio" class="c_radio" name="npsMark" data-mark="<?php echo $i ?>" ></div>
						<div class="mb_label"><label><?php echo $i ?></label></div>	
						</div>
			
					<?php endforeach; ?>

				</form>


			</div>	
			<div class="nps_popup_succes">
			Спасибо за ответ! :)

				</div>
			<div class="c_button_row">
				<button class="btn btn-success nps_popup_submit">Отправить</button>
				<button class="btn btn-danger nps_popup_close">Закрыть</button>
				<button class="btn nps_popup_hide">Скрыть</button>
			</div>	
			<br>
			<div class="nps_error" style="display: none;">Оценка не выбрана</div>
			






		</div>
		
	</div>
	


</div>


 <script >
	 window = window || {};    
	 window.__app__ =  window.__app__ || {}; 
	 window.__app__.npsPopup =  window.__app__.npsPopup || {};  
	 window.__app__.npsPopup.id = +"<?php echo $model->id ;?>";
	 window.__app__.npsPopup.timeout = +"<?php echo $model->npsTimeout ;?>";


 	(function($){
 	
function popup_hide(){
	$(".nps_popup").hide();
}

$(".nps_popup_close").on('click', function(){
	
	$.ajax({
		method: 'POST',
		url: window.ajaxurl,
		data: {
			action: "npsPopupStatus",
		},
		success: popup_hide()


	});
});

$(".nps_popup_hide").on('click',function(){
	popup_hide();
})
function nps_popup_succes_hide(){
	$(".c_range_row").hide();
	$(".nps_popup_succes").show();
	setTimeout(function(){

		$(".nps_popup").hide('400');
	}, 800);
}



$(".nps_popup_submit").on('click', function(){
	var npsId = $(".c_title").attr("data-id"); 
	var npsMark = $('input[name=npsMark]:checked', '#popupForm').attr("data-mark");
	if($("input[name=npsMark]").is(':checked')){
		$(".nps_error").hide();
		$.ajax({
	      method: 'POST',
	      url: window.ajaxurl,
	      data: {
	      	action: "npsPopupSubmit",
	      	id: npsId,
	      	mark: npsMark
	      },
	      succes: nps_popup_succes_hide()
		});
	} else{
		$(".nps_error").show();
	}

	
})



 	})(jQuery);

 	

</script> 