<?php 
	if(isset($_REQUEST['showdetail'])){
		return include ('pashkov-nps-admin-detail.php');
	}
	$dir = plugin_dir_path( __FILE__ );
	$current_url =  $_SERVER['REQUEST_URI'];
	if(isset($_POST['npsTitle']) && isset($_POST['npsMaxRange'])){
	$npsTitle = $_POST['npsTitle'];
	$npsMaxRange = $_POST['npsMaxRange'];
	$npsType = $_POST['npsType'];
	if($npsType === 'Popup'){
		$npsTimeout = $_POST['npsTimeout'];
	}else{
		$npsTimeout = 0;
	}
	$npsVisible = isset($_POST['npsVisible']) ? true:false;

	if(isset($npsTitle)){
		$npsadd = new NpsList();
		$npsadd->npsTitle = $npsTitle;
		$npsadd->npsMaxRange = $npsMaxRange;
		$npsadd->visible = $npsVisible;
		$npsadd->npsTimeout = $npsTimeout;
		$npsadd->type = $npsType;
		$npsadd->save();
		}
	}	

?>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style type="text/css">
	span.npsbtn{
		cursor: pointer;
		color: #3a6dc1;
	}
</style>

<br>

<div class="list">
	<h2>Создать опрос</h2>

<form method="POST" action="">
	Вопрос: <input type="text" name="npsTitle" required> 
	Максимальная оценка: <input type="text" name="npsMaxRange" style="width: 50px"  required> 
	<select name="npsType" class="npsTypeSelect">
  <option value="Popup">Popup</option>
  <option value="Shortcode">Shortcode</option>
</select >
	Задержка: <input type="text" style="width: 50px" name="npsTimeout">
	Активен: <input class="npsVisible" type="checkbox" name="npsVisible" checked>
	
	<button type="submit" class="btn" >Добавить</button>

</form>



<br>
<h2 >Список опросов</h2> 
<table class="table table-hover">
	<thead>
		<tr>
			<td><b>ID</b></td>
			<td><b>Опрос</b></td>
			<td><b>Макс. оценка</b></td>
			<td><b>Задержка</b></td>
			<td><b>Тип</b></td>
			<td><b>Статус</b> <span class="npsbtn dashicons dashicons-visibility multiple_status" style="cursor: pointer;"></span><span class="npsbtn dashicons dashicons-yes multiple_status_submit" style="display: none;"></span></td>
			<td class="multiple_status_checkbox" style="display: none;"></td>
			<td><b>Дата создания</b></td>
			<td></td>

		</tr>
	</thead>
	<tbody>
	<?php
		$npslist = NpsList::query()->get();
		foreach ($npslist as $i): ?>
		<tr>
			<td><?php echo $i->npsId ?></td>
			<td><a href="<?php echo $current_url."&showdetail=".$i->npsId ?>" class="npsQuestDetail" data-id="<?php echo $i->npsId ?>"><?php echo $i->npsTitle ?></a></td>
			<td><?php echo $i->npsMaxRange ?></td>
			<td><?php echo $i->npsTimeout ?></td>
			<td><?php echo $i->type ?></td>
			<td><?php if(($i->visible) === 0){echo 'Неактивен';}else{echo 'Активен';} ?></td>
			<td class="multiple_status_checkbox" style="display: none;">
				<input data-id="<?php echo $i->npsId ?>" data-check="<?php echo $i->visible ?>" type="checkbox" class="qEditCheckboxM" name="" <?php if(($i->visible)===1){echo 'checked';}  ?>> 
			</td>
			<td><?php echo $i->npsCreateDate ?></td>
			<td><span  data-id="<?php echo $i->npsId ?>" data-title="<?php echo $i->npsTitle ?>" class="npsbtn dashicons dashicons-edit npsQuestEditS"></span>
				<span data-id="<?php echo $i->npsId ?>" class="npsbtn dashicons dashicons-trash npsQuestDelete"></span></td>
		</tr>
		<tr class="qEdit" data-id="<?php echo $i->npsId ?>" style="display: none;" >
			<td></td>
			<td><input type="text" name="" data-id="<?php echo $i->npsId ?>" class="qEditTitle" value="<?php echo $i->npsTitle ?>"></td>
			<td><input type="text" class="qEditMaxRange" name="" size="1" value="<?php echo $i->npsMaxRange ?>"  data-id="<?php echo $i->npsId ?>"> </td>
			<td><input type="text" class="qEditTimeout" name="" size="1" value="<?php echo $i->npsTimeout ?>" data-id="<?php echo $i->npsId ?>"></td>
			<td>
					<select  class="npsTypeSelectE" style="width: 80px" data-id="<?php echo $i->npsId ?>">
				  <option value="<?php if($i->type === 'Popup'){echo 'Popup';}else{echo 'Shortcode';} ?>"><?php if($i->type === 'Popup'){echo 'Popup';}else{echo 'Shortcode';} ?></option>
				  <option value="<?php if($i->type === 'Popup'){echo 'Shortcode';}else{echo 'Popup';} ?>"><?php if($i->type === 'Popup'){echo 'Shortcode';}else{echo 'Popup';} ?></option>
					</select >

			</td>
			<td>  <input data-id="<?php echo $i->npsId ?>" data-check="<?php echo $i->visible ?>" type="checkbox" class="qEditCheckbox" name="" <?php if(($i->visible)===1){echo 'checked';}  ?>> </td>
			<td><button class="btn btn-success small btn-sm npsQuestEdit" data-id="<?php echo $i->npsId ?>" data-title="<?php echo $i->npsTitle ?>" data-range="<?php echo $i->npsMaxRange ?>">Обновить</button></td>
			<td></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	

</table>


</div>


 <script >
 	(function($){
 		function reload(){
 		window.location.reload();
 		}


 	$(".multiple_status").on('click', function(){
 		$(".multiple_status_checkbox").toggle();
 		$(".multiple_status_submit").toggle();
 	})
 	$(".multiple_status_submit").on('click', function(){
 		var count = $('.qEditCheckboxM').length;
 		var id_arr = [];
 		var visible_arr = [];
 		for(var i=0; i < count; i++){
			if($(".qEditCheckboxM:eq("+i+")").prop('checked')===true){
				var npsVisible = 1;
			} else {
				var npsVisible = 0;
			}
			var npsId = $(".qEditCheckboxM:eq("+i+")").attr("data-id");

			visible_arr.push({id: npsId, visible: npsVisible});
	
 		}
 		
 		$.ajax({
 			method: 'POST',
 			url: window.ajaxurl,
 			data: {
 				action: 'npsPopupSubmitMultiplee',
 				visible: visible_arr,
 			},
 			success: reload()
 		})
 	})
	$(".npsQuestEditS").on('click', function(){
		var npsId = $(this).attr("data-id");
		$(".qEdit[data-id="+ npsId + "]").toggle('fast');

	})

	$(".npsTypeSelect").change(function(){
		if($(".npsTypeSelect").val() == 'Shortcode'){
			$("input[name=npsTimeout]").prop('disabled', true);
			$("input[name=npsTimeout]").val("0");
		} else{
			$("input[name=npsTimeout]").prop('disabled', false);
			$("input[name=npsTimeout]").val("");
		}
	})

	$(".npsQuestDetail").on('click', function(){
		var npsId = $(this).attr("data-id");
		$.ajax({
			method: 'POST',
			url: window.ajaxurl,
			data: {
				action: "npsQuestDetail",
				id: npsId,
	      },


		});

	})

	$(".npsQuestEdit").on('click', function(){
		var npsId = $(this).attr("data-id");
		// var npsTitle = $(this).atrr("data-title");
		var npsTitle = $(".qEditTitle[data-id="+ npsId + "]").val();
		var npsMaxRange = $(".qEditMaxRange[data-id="+ npsId + "]").val();
		var npsTimeout = $(".qEditTimeout[data-id="+ npsId + "]").val();
		var npsType = $(".npsTypeSelectE[data-id="+ npsId + "]").val();
		// var npsVisible = $(".qEditCheckbox[data-id="+ npsId + "]").attr("data-check");
		if($(".qEditCheckbox[data-id="+ npsId + "]").prop('checked')===true){
			var npsVisible = 1;
		} else {
			var npsVisible = 0;
		}
		$.ajax({
	      method: 'POST',
	      url: window.ajaxurl,
	      data: {

	      	action: "npsQuestionUpdate",
	      	id: npsId,
	      	title: npsTitle,
	      	maxrange: npsMaxRange,
	      	visible: npsVisible,
	      	timeout: npsTimeout,
	      	type: npsType
	      },
	      succes: reload()
	  
		});
	})

	$(".npsQuestDelete").on('click', function(){
		var npsId = $(this).attr("data-id");
		// var npsTitle = $(this).atrr("data-title");
		$.ajax({
	      method: 'POST',
	      url: window.ajaxurl,
	      data: {
	      	action: "npsQuestionDelete",
	      	id: npsId
	      },
	      succes: reload()
	  
		});
	})
 	})(jQuery);

 	

</script> 


