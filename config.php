<?php
return [
	'db' => array(
	'driver' => 'mysql',
	'host' => 'localhost',
	'database' => 'wordpress2',
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	'collation' => 'utf8_general_ci',
	'prefix' => '',
	),
];
