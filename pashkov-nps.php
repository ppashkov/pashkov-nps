<?php 

/*
 * Plugin Name: pashkov-nps
 * Description: Плагин для создание опросов.
 * Author: PASHKOV
 */


error_reporting(E_ALL);
mb_internal_encoding("UTF-8");

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$config=require_once 'config.php';
\tpf\utils\Db::initConnection($config['db']);
$con=tpf\utils\Db::getConnection();

register_activation_hook( __FILE__, 'myplugin_activate' );

function myplugin_activate() {


if(!DB::schema()->hasTable('pashkov_nps_list')){
	 DB::statement('CREATE TABLE pashkov_nps_list (
	npsId INT(6) AUTO_INCREMENT PRIMARY KEY,
	npsTitle VARCHAR(255) NOT NULL,
	npsTimeout INT(6) NOT NULL,
	npsMaxRange INT(6),
	type VARCHAR(255) NOT NULL,
	visible INT(11),
	npsCreateDate TIMESTAMP
	)');	
}
if(!DB::schema()->hasTable('pashkov_nps_answers')){
	 DB::statement('CREATE TABLE pashkov_nps_answers (
	id INT(6) AUTO_INCREMENT PRIMARY KEY,
	npsId INT(6) NOT NULL,
	npsAnswer INT(6) NOT NULL,
	npsAnswerDate TIMESTAMP
	)');
}






}

add_action('admin_menu', function(){

	add_menu_page('PASHKOV NPS', 'Опросы',  'manage_options', 'pashkov-nps/pashkov-nps-admin-list.php', '', '
	dashicons-groups', '1');
	if ( function_exists ( 'add_menu_page' ) ) {
		add_submenu_page('pashkov-nps/pashkov-nps-admin-list.php', 'PASHKOV NPS - Добавить', 'Ответы', 'manage_options', 'pashkov-nps/pashkov-nps-admin-answers.php');
		// add_submenu_page('pashkov-nps/pashkov-nps-admin-list.php', 'PASHKOV NPS - Опросы', 'Подробней', 'manage_options', 'pashkov-nps/pashkov-nps-admin-detail.php');
	}

});

add_action('wp_enqueue_scripts', function(){
	if ( !is_admin() ) { 
		wp_enqueue_script( 'jquery' );
	}

});


add_action('wp_ajax_nopriv_npsQuestionUpdate', 'npsQuestionUpdate');
add_action('wp_ajax_npsQuestionUpdate', 'npsQuestionUpdate');

function npsQuestionUpdate(){
	$npsId = $_REQUEST['id'];
	$npsTitle = $_REQUEST['title'];
	$npsMaxRange = $_REQUEST['maxrange'];
	$npsVisible = $_REQUEST['visible'];
	$npsTimeout = $_REQUEST['timeout'];
	$npsType = $_REQUEST['type'];
	$questUpdate = NpsList::find($npsId);
	$questUpdate->npsTitle = $npsTitle;
	$questUpdate->npsMaxRange = $npsMaxRange;
	$questUpdate->visible = $npsVisible;
	$questUpdate->npsTimeout = $npsTimeout;
	$questUpdate->type = $npsType;
	$questUpdate->save();

	die();
}

add_action('wp_ajax_nopriv_npsQuestionDelete', 'npsQuestionDelete');
add_action('wp_ajax_npsQuestionDelete', 'npsQuestionDelete');

function npsQuestionDelete(){
	$npsId = $_REQUEST['id'];
	NpsList::destroy($npsId);
	die();
};


add_action('wp_head', 'nps_popup_show');
function nps_popup_show(){
	?>	

	<?php 
	include('nps_popup.php')
	 ?>


	<script>

			setTimeout(function(){

		jQuery(".nps_popup").show();
	
	}, window.__app__.npsPopup.timeout * 1000)
		
		alert(window.npsShortcode.vis);

	
	</script>
	<?php 
}



add_action('wp_ajax_nopriv_npsPopupSubmit', 'npsPopupSubmit');
add_action('wp_ajax_npsPopupSubmit', 'npsPopupSubmit');

function npsPopupSubmit(){
	SetCookie("nps-status", "0",  time()+(60*60)*9, COOKIEPATH, COOKIE_DOMAIN);
	$_COOKIE['nps-status'] = "0";
	$npsId = $_REQUEST['id'];
	$npsMark = $_REQUEST['mark'];
	$npsAnswer = new NpsAnswers();
	$npsAnswer->npsId = $npsId;
	$npsAnswer->npsAnswer = $npsMark;
	$npsAnswer->save();
	$npsIdCookie = "nps"."-".$npsId;

	if(isset($_COOKIE['nps-data'])){
		$cookie = $_COOKIE['nps-data'];
		$cookie = stripslashes($cookie);
		$npsData = json_decode($cookie);

	} else{
		$npsData = array();
	}

	if(!in_array($npsId, $npsData)){
		array_push($npsData, $npsId);
		$json_data = json_encode($npsData);
		SetCookie("nps-data", $json_data,  time()+(60*60)*24*365*2, COOKIEPATH, COOKIE_DOMAIN);
		$_COOKIE['nps-data'] = $json_data;

	}

	die();
};

add_action('wp_ajax_nopriv_npsPopupStatus', 'npsPopupStatus');
add_action('wp_ajax_npsPopupStatus', 'npsPopupStatus');

function npsPopupStatus(){
	SetCookie("nps-status", "0", time()+(60*60)*27, COOKIEPATH, COOKIE_DOMAIN);
	$_COOKIE['nps-status'] = "0";
	die();
};

add_action('wp_ajax_nopriv_npsPopupSubmitMultiplee', 'npsPopupSubmitMultiplee');
add_action('wp_ajax_npsPopupSubmitMultiplee', 'npsPopupSubmitMultiplee');

function npsPopupSubmitMultiplee(){

	foreach ($_REQUEST['visible'] as $i) {
		$npsMultipleeUpdate = NpsList::find($i['id']);
		$npsMultipleeUpdate->visible = $i['visible'];
		$npsMultipleeUpdate->save();

	}

	die();
};


if(isset($_REQUEST['exportcsv'])){
	$csvAns = NpsAnswers::query()->where('npsId', '=', $_REQUEST['exportcsv'])->get();
	$csvfind = NpsList::query()->find($_REQUEST['exportcsv']);
	$csvTitle = $csvfind->npsTitle;
	$csvMaxRange = $csvfind->npsMaxRange;
	$npsId = $_REQUEST['exportcsv'];
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename='nps-$npsId.csv'");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "Title,Max,Answer,Date\n";
	foreach ($csvAns as $i) {
		
		echo "$csvTitle, $csvMaxRange, $i->npsAnswer, $i->npsAnswerDate\n";

	}
	die;
}


add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {
    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

function p_nps_func( $atts ){
	 
	 ob_start();
	 $scId = isset($atts['id']) ? $atts['id']:null;
     include('nps_shortcode.php');
     $output = ob_get_clean();
     return $output;

}
 
add_shortcode( 'p_nps', 'p_nps_func' );
